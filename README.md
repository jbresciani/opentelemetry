# Opentelemetry, grafana, tempo, jenkins and gradle

This project will attempt to setup transmission of jenkins opentelemetry data and gradle build opentelemetry data as a single trace and present it in grafana tempo

# requirements

Docker

# How to

All components are configured to run from docker compose, checkout this repository and run the following command from the root of the checked out project:

```bash
docker compose up
```

This will build and launch Jenkins on http://localhost:8080 and Grafana on http://localhost:3000. both URL's autologin as admin. Tempo and prometheus will be launched to track traces and metrics and will be automatically setup and the telemetry data sent to Tempo. (Grafana Loki for logging is not configured at this time)

To stop destroy the containers hit CTRL+c to stop docker compose and then run:

```bash
docker compose down
```

To remove the stopped containers.

Changes to the jenkins configuration requires destroying the local image before restarting docker compose. 

```bash
docker rmi opentelemetry-jenkins:latest
```

should do that for you.

## Jenkins

Jenkins is pre-built with the opentelemetry and configuration-as-code plugins installed, the opentelemetry plugin will be pre-configured to send traces and metrics to the tempo container for visualization through grafana. The configuration of Jenkins can be found in the ./jenkins-docker/casc.yaml file. 

There will be one pre-built job called `gradle` (found at http://localhost:8080/job/gradle in the UI and ./jenkins/jobs/gradle in the repo), that runs `gradlew jibBuildTar` but abandon's the artifact once the job is complete.

Build history and logs are stored in ./jenkins/jobs/ so they will survive the destruction of the container by a `docker compose down`

## Grafana, Tempo, Prometheus

Grafana, Tempo and Prometheus (for trace metrics storage) are all configured as per the docker-compose example here https://github.com/grafana/tempo/tree/main/example/docker-compose/local and with the configs found here https://github.com/grafana/tempo/tree/main/example/docker-compose/shared but all configuration has been mirrored into this repo to reduce random bugs from changes from being introduced to this example.

Tempo data will be writen to local disk at ./tempo-data. so trace data will survive a the destruction of the containers. Prometheus metrics are not currently stored off the container so that data will be lost if `docker-compose down`.

## TODO

- version lock all container versions
- version lock jenkins plugin versions
- jenkins build history/logs/results stored off container
- prometheus db stored off the container 