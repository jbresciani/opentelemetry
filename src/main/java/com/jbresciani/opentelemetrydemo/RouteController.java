package com.jbresciani.opentelemetrydemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import io.opentelemetry.instrumentation.annotations.SpanAttribute;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.StatusCode;
import io.opentelemetry.api.common.Attributes;
import io.opentelemetry.api.common.AttributeKey;

@RestController
public class RouteController {

    // WithSpan turns this function into a span name <class>.<function>
    @WithSpan
    // SpanAttribute injects this paramater as an attribute
    public String doTraceStuff (@SpanAttribute("service.opentelemetryData.doTraceStuff.nameParam") String name) {       
        // fetch the traceId from the current span for later use
        String traceId = Span.current().getSpanContext().getTraceId();

        // creates an attribute key and value of your choosing to be added to this span
        Span.current().setAttribute("service.opentelemetrydemo.traceFailureCheck", "nope");
        Span.current().setAttribute("service.opentelemetrydemo.kaboomCheck", "nope");

        Attributes eventAttributes = Attributes.of(
            AttributeKey.stringKey("key"), "value",
            AttributeKey.longKey("result"), 0L);
        // this adds in an extra span event under the first fields name (`computation log results` in this case), think of it as
        // an alternative to a log file that is included with your trace data. it can be used for everyday logging, or expanding
        // on an exception
        Span.current().addEvent("computation log results", eventAttributes);

        if ("kaboom".equals(name)) {
            // exceptions should show up on spans as events and, hopefully, automatically set the span to an
            // error status
            Span.current().setAttribute("service.opentelemetrydemo.kaboomCheck", "yup");
            throw new RuntimeException("name == kaboom so Kaboom!");
        } else if ("traceFailure".equals(name)) {
            // something goes here to set span status to error
            Span.current().setAttribute("service.opentelemetrydemo.traceFailureCheck", "yup");
            Span.current().setStatus(StatusCode.ERROR, "name == traceFailure and therefore we set the span to error.");
        }
        return traceId;
    }
    @GetMapping("/traceFailure")
    public TraceFailure traceFailure(@RequestParam(value = "name", defaultValue = "traceFailure") String name) {
        String template = "Hello from /traceFailure, %s!";
        String traceId = doTraceStuff(name);
        return new TraceFailure(traceId, String.format(template, name));
    }
    @GetMapping("/kaboom")
    public Kaboom kaboom(@RequestParam(value = "name", defaultValue = "kaboom") String name) {
        String template = "Hello from /kaboom, %s!";
        String traceId = doTraceStuff(name);
        return new Kaboom(traceId, String.format(template, name));
	}
    @GetMapping("/")
    public Slash slash(@RequestParam(value = "name", defaultValue = "World") String name) {
        String template = "Hello from /, %s!";
        String traceId = doTraceStuff(name);
        return new Slash(traceId, String.format(template, name));
	}
}