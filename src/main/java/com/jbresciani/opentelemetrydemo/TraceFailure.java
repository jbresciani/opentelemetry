package com.jbresciani.opentelemetrydemo;

public record TraceFailure(String id, String content) { }
