package com.jbresciani.opentelemetrydemo;

public record Slash(String id, String content) { }
