package com.jbresciani.opentelemetrydemo;

public record Kaboom(String id, String content) { }
